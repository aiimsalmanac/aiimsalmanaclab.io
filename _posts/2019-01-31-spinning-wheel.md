---
layout: post
title:  "The Gandhian Philosophy of Spinning-Wheel"
author: S. Narayana Swami
author-bio: ""
author-avatar: assets/images/m.png
author-twitter: https://twitter.com/the_hindu
categories: [ Gandhi, Philosophy ]
image: https://images.unsplash.com/photo-1523740856324-f2ce89135981?ixlib=rb-1.2.1&auto=format&fit=crop&w=798&q=80
tags: []
---

The book “The Gandhian Philosophy Of Spinning-Wheel” is indeed a golden book published on the eve of the Golden Jubilee of Indian Republic in the new millennium. Professor Mohit Chakrabarti has made a commendable effort to trace out the philosophical background of the spinning-wheel and to highlight the various aspects of the spinning wheel from the Gandhian angle.

The book is strengthened by seven chapters running into about 105 pages, making a thin bound small volume dedicated two different Gandhian educational thinkers, Dr. Mahendra Kumar, Editor of Gandhi Marg, and Professor Dilip Kumar Sinah, Vice-Chancellor of Vishwa Bharati University, Shanti Niketan, created by Rabindranath Tagore. It is obvious that the author of the book has not only sought blessings but has also borrowed their ideas to design this book. It is obviously based on the library research with the original writings of Mahatma Gandhi and on the secondary sources. Besides, a creative thinking of the author is also reflected through the figures and diagrams, highlighting the paramount important of the spinning-wheel which serves as a focal point of the child’s growth and development of personality.

To begin with, the author makes an attempt to spell out the aims and objectives of the spinning-wheel in the first chapters. As an inward sprit, the spinning-wheel, as Mahatma Gandhi introspects, epitomizes man as a divine being. As an outward spirit, it emphasizes self-help, self-service, self-contentment, and austerity. The dream of the spinning-wheel, as he visualizes, is the dream of a better emancipation of man as an individual and social being.

In the second chapter, Gandhian perspectives of education and the spinning-wheel are analysed. The analysis reveals that the educational system can be shaped or refined through a craft and that the spinning-wheel can play a very significant role so as to make education self-reliant and self-supporting.

The third chapter, titled “Soceity and the spinning-wheel”, clearly indicates that the spinning-wheel binds the heart of everyone in society with the common cord of social oneness. The seeds of national and social cohesion can be sown through the music of the spinning-wheel. However, the author emphatically argues that true Swaraj and prosperity of society can be visualized and realized only through the spinning-wheel being the centre of all handicraft.

The concept of nonviolence and the spinning-wheel is discussed in chapter four. The author points out that the concept of sacrifice well nurtured in Gandhi’s concept of nonviolence and the spinning-wheel is also at the root of his concept of the spinning-wheel. When Gandhi associates nonviolence with the spinning-wheel, he actually makes a unison of worship with work and he considers the spinning-wheel the best symbol of nonviolence.

An eleven-page scholarly analysis of the book under chapter five highlights the religious dimensions of the spinning-wheel. It instills the true concept of religion by means of the spinning-wheel which Gandhi calls “Sarvadharma Samanatva”. This chapter clearly indicates that Gandhi is always in favour of a new religion that teaches how to safeguard and enrich self-respect and self-development of each individual in order to safeguard and enrich national honour and national development.

The sixth chapter deals with the humanistic spirit of the spinning-wheel. At the outset, the author records that the spinning-wheel is the true symbol of humanism and it is an effective vehicle to serve as a spurt both inwardly and outwardly, directly and indirectly. Plain living or living nobly as true humanism always aims at is the sine qua non of the spinning wheel. Further, this chapter obviously reveals the four significant reasons why Gandhi advocates the humanistic spirit of the spinning-wheel.

In the last chapter, the extraordinary power of the wheel is indicated under the caption “postscript”. It is soul-stirring to record the fact that Mahatma Gandhi expressed his desire that he craved to die with his hand at the spinning-wheel. Further, he wanted to change the very attitude of man towards man and society and vice versa by introducing the spinning-wheel.

There is no doubt that this book will change the minds of the younger generations and the attitude of the people in the new millennium and the centuries to come. On the whole, Mohit Chakrabarti’s efforts deserve full praise, especially for his deft handling of both primary and secondary sources.

The book is a scholarly piece of work, which deserves careful reading by social scientists, planners, and administrators, especially at a time when the love for auto machines and electronic goods is increasing and new perspectives as well as alternatives are being heralded. It will interest all those working in the area of Gandhian studies, government, voluntary sector, and industrial units occupied by modern machinery without a philosophy.
