---
layout: post
title:  "Post with YouTube video"
author: Jeremy
author-bio: "Jeremy is an awesome dude. Despite his delusions of grandeur, he is harmless. Will bite if you steal his chocolates"
author-avatar: assets/images/m.png
author-twitter: https://twitter.com/the_hindu
categories: [ tutorial ]
image: assets/images/14.jpg
tags: [featured]
---

The following snipper will embed youtube video of the corressponding address.
```html
<p><iframe style="width:100%;" height="315" src="https://www.youtube.com/embed/Cniqsc9QfDo?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>
```

<p><iframe style="width:100%;" height="315" src="https://www.youtube.com/embed/Cniqsc9QfDo?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></p>

## Lorem ipsum dolor sit amet
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at sem vestibulum, sodales odio vitae, mollis nibh. Cras congue nunc vitae augue sollicitudin cursus. Nullam quis odio sapien. Curabitur eu ultricies justo. Aenean felis neque, aliquet quis nulla in, ultrices sollicitudin tellus. Nunc luctus tortor congue velit rhoncus auctor. Mauris maximus, justo eu dapibus porta, nulla ante dignissim enim, a tempor nulla augue at metus. In rhoncus orci vitae sagittis luctus. In ac cursus urna. Integer non ante mauris. Fusce bibendum sed tortor vitae laoreet. Sed in eleifend sem. Ut vestibulum euismod tellus, et placerat nulla sollicitudin eget. Aenean nec elit vel dolor mattis molestie vitae nec dui. Mauris sodales tincidunt est.

Etiam vitae arcu sed nulla tempor interdum. Nulla nec sagittis turpis. Nam metus sapien, venenatis at mi eget, suscipit tincidunt odio. Mauris ut venenatis enim, et commodo enim. Maecenas varius auctor neque, ac semper lorem commodo ut. Morbi ut luctus quam. Duis in ipsum a lacus faucibus porta at non diam. Nulla ullamcorper vulputate leo, ac mollis nulla tempor porttitor. Aliquam eu sem lacinia, posuere mi et, ornare enim.

Cras quis augue nulla. Maecenas porttitor, magna vel suscipit venenatis, nulla dui ultricies nisl, eu sodales libero lectus quis ante. Sed vestibulum fermentum nisi, sed finibus magna congue eget. Suspendisse dapibus urna sit amet semper vulputate. Proin feugiat interdum ex, nec facilisis urna venenatis at. Vivamus aliquam id leo et elementum. Donec faucibus, dui at vestibulum auctor, justo diam mollis massa, vitae suscipit mi tortor ut lacus. Donec rhoncus nibh ac orci volutpat, et consectetur lacus ultrices. Aenean molestie luctus tempus. Sed condimentum quis tortor eleifend placerat. Donec imperdiet cursus tristique.

Donec tellus nisi, ultrices eu lacus eu, gravida dapibus leo. Cras at dui nec felis aliquet mattis a eget mauris. Mauris ornare est ac luctus finibus. Proin elementum at lectus sit amet molestie. Mauris tempor ex urna, nec interdum nunc finibus a. Etiam elementum ornare fermentum. Sed mattis lorem id lorem faucibus tempus. Maecenas non cursus tellus.

## Nulla vitae sodales justo
Nulla vitae sodales justo. Integer ultricies sollicitudin ex ac tempor. Proin et arcu sed lorem mattis gravida. Duis dapibus metus lectus, fringilla interdum nisl dapibus nec. Morbi vestibulum rutrum nulla, auctor varius ante eleifend nec. Nullam malesuada nibh et est mollis ornare. Quisque ultrices augue ut eleifend ultricies. Integer sit amet mauris nunc. Vivamus sollicitudin dictum ligula ac eleifend. Vivamus at suscipit nisl. 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at sem vestibulum, sodales odio vitae, mollis nibh. Cras congue nunc vitae augue sollicitudin cursus. Nullam quis odio sapien. Curabitur eu ultricies justo. Aenean felis neque, aliquet quis nulla in, ultrices sollicitudin tellus. Nunc luctus tortor congue velit rhoncus auctor. Mauris maximus, justo eu dapibus porta, nulla ante dignissim enim, a tempor nulla augue at metus. In rhoncus orci vitae sagittis luctus. In ac cursus urna. Integer non ante mauris. Fusce bibendum sed tortor vitae laoreet. Sed in eleifend sem. Ut vestibulum euismod tellus, et placerat nulla sollicitudin eget. Aenean nec elit vel dolor mattis molestie vitae nec dui. Mauris sodales tincidunt est.

Etiam vitae arcu sed nulla tempor interdum. Nulla nec sagittis turpis. Nam metus sapien, venenatis at mi eget, suscipit tincidunt odio. Mauris ut venenatis enim, et commodo enim. Maecenas varius auctor neque, ac semper lorem commodo ut. Morbi ut luctus quam. Duis in ipsum a lacus faucibus porta at non diam. Nulla ullamcorper vulputate leo, ac mollis nulla tempor porttitor. Aliquam eu sem lacinia, posuere mi et, ornare enim.

Cras quis augue nulla. Maecenas porttitor, magna vel suscipit venenatis, nulla dui ultricies nisl, eu sodales libero lectus quis ante. Sed vestibulum fermentum nisi, sed finibus magna congue eget. Suspendisse dapibus urna sit amet semper vulputate. Proin feugiat interdum ex, nec facilisis urna venenatis at. Vivamus aliquam id leo et elementum. Donec faucibus, dui at vestibulum auctor, justo diam mollis massa, vitae suscipit mi tortor ut lacus. Donec rhoncus nibh ac orci volutpat, et consectetur lacus ultrices. Aenean molestie luctus tempus. Sed condimentum quis tortor eleifend placerat. Donec imperdiet cursus tristique.

Donec tellus nisi, ultrices eu lacus eu, gravida dapibus leo. Cras at dui nec felis aliquet mattis a eget mauris. Mauris ornare est ac luctus finibus. Proin elementum at lectus sit amet molestie. Mauris tempor ex urna, nec interdum nunc finibus a. Etiam elementum ornare fermentum. Sed mattis lorem id lorem faucibus tempus. Maecenas non cursus tellus.

Nulla vitae sodales justo. Integer ultricies sollicitudin ex ac tempor. Proin et arcu sed lorem mattis gravida. Duis dapibus metus lectus, fringilla interdum nisl dapibus nec. Morbi vestibulum rutrum nulla, auctor varius ante eleifend nec. Nullam malesuada nibh et est mollis ornare. Quisque ultrices augue ut eleifend ultricies. Integer sit amet mauris nunc. Vivamus sollicitudin dictum ligula ac eleifend. Vivamus at suscipit nisl. 

