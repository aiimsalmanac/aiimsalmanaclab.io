---
layout: post
title:  "Kingian Nonviolence : A practical application in policing"
author: Capt. Charles Alphin
author-bio: "Capt. Charles Alphin, who grew up in a culture of violence, drugs and gangs was won over to nonviolence by Dr Bernard LaFayette, another associated of Dr King. Today he is a key figure in training 50,000 police officers in nonviolent methods in USA."
author-avatar: assets/images/m.png
author-twitter: https://twitter.com/the_hindu
categories: [ Non-violence ]
image: assets/images/6.jpg
tags: []
---

> Nonviolence is a powerful and just weapon. It is a weapon unique in history, which cuts without wounding, and ennobles the man who wields it. It is a sword that heals - Martin Luther King, Jr.


When Dr. King was assassinated in April 1968, I was a detective of three years experience with the St. Louis Police Department, St. Louis, Missouri. Having joined the police department in July 1965, I was relatively new to law enforcement. 
By this time I was very angry with the Criminal Justice System because I had experienced racism, unfair treatment, dual standards, selective law enforcement and lack of sensitivity for poor people. As I listened to other police officers, especially African American officers, I understood that this was not unique to St. Louis, but common in most urban areas throughout the United States.I remember when the Chief of Police in St. Louis announced that all St. Louis Police Officers would wear black arm bands during a memorial march in Dr. King's honor.Some white officers refused to obey the Chief's order and did not leave the police station. I guess I could understand their resistance to participate, but their open display of racism deeply disturbed me.In addition, the supervisors did not take any action against the officers.This reinforced my belief of the dual system within the police department.

I remember this conflict percolated a lot of emotions between black and white officers for months and years that followed. As I look back I would admit that I did not handle the conflict very well, and I am certainly not proud of the way I confronted the white officers.

Never in my wildest imagination would I have imagined that I would embrace Dr. King's philosophy and incorporate it into an effective way of policing. I did not understand or support Dr. King nor think that nonviolence was effective in changing situations or persons. I thought it was a passive and do-nothing strategy, pray and wait for the Lord to change things.How terribly wrong I was.In a time span just short of thirty years I have experimented with the Kingian Philosophy in all phases of policing in a democratic society, taught and dialogued about the application of Kingian Philosophy in policing with three governments:  

1. Communist'Soviet Union, Moscow, Leningrad, Tashkent, Sandmarkan and Lithuania, 1990 and 1991
2. Military Dictator - Haiti, General Cedras, 1993
3. Totalitarian - South Africa, 1993, '94, '95, '96. '97 

My initial contact with these governments occurred while they were in the process of changing, however, they were still very receptive and the dialogue with them continues.

Although the Kingian Philosophy may be applied to dictatorial situations and governments, this paper will only focus on policing in a democratic society. My policing experience started on July 21, 1965, when I was sworn in as a police officer with the St. Louis Police Department.During my 26 plus years as a police officer I was promoted three times, Sergeant, Lieutenant, and Captain. The last 10 years of my career I served in the rank of Captain, as

1. District Commander 
2. Commander of Homicide/Rape and Child Abuse
3. Commander of Vice-Narcotics

My educational experience in St. Louis consisted of my association with only blacks, however my religious experience consisted of my association with all cultures and races.My grandfather was a minister and we were members of the Centennial Christian Church.

My youth consisted of a lot of violence. I belonged to a 'gang' and participated in a lot of fighting, from turf conflicts to girls.My environment taught me that the way to solve conflict was through fighting. 

I took this same attitude to the St. Louis police Department and soon found out that most of my associates had the same attitude.Frequently they hid behind the 'gang' of the police.As I observed police service and protection provided to some communities my attitude deepened concerning the effective working of the Criminal justice System. 
When I was a Sergeant, my wife and I became involved in an educational conflict with the high school where both my sons attended. The conflict was concerning academics and sports, as both of my sons played three sports for the high school, football, baseball and basketball. Our family rules were that if our children were not doing their best in academics, they could not participate in sports.One afternoon I received in the mail a notice that my oldest son was failing in one of his subjects.We removed him from the football team and several school officials called me to ask me to reconsider my decision,stating that the upcoming game was an important game and they needed him.After refusing to reconsider, my wife and I attended an angry parents meeting of about 60 parents, all complaining about the academics among African American students in the school system. 

At the meeting I met Dr. Bernard LaFayette, Jr., who had a son in the high school, and was Associate Minister of the Baptist Church of the Holy Communion. Dr. LaFayette sat in the rear of the room and listened to the angry parents, and then began to ask questions.Based on his questions, we came up with a strategy to address the problem with the school system. In future meetings he continued to lead the group by asking questions, and making suggestions when he felt they were appropriate.It is interesting to recall that at no time did he mention to the group or me his doctorate degree from Harvard University, nor his tremendous experience with Dr. King and the Civil Rights Movement.Later in my readings I would find his name in most books that gave an account of the Civil Rights Movement.

After the school issue was addressed, we, for several years, continued working together on other issues in the community.In 1981 he sponsored a 'Freedom Ride', to commemorate twenty years since the original '1961 Freedom Rides.'It was on this 'Freedom Ride' in Selma, Alabama that I learned who Dr. Bernard LaFayette, Jr. was, my friend of several years.When the political leadership presented him with the 'Key' to the city, and I heard the changes that happened as a result of the nonviolent strategy, I felt a tremendous thirst to learn all that I could about this method of fighting.
During the 'Freedom Ride', I questioned Dr. LaFayette's friends such as James Farmer, Honorable Andrew Young, Dorothy Cotton, Rev. C.T. Vivian, David Jehnsen, Congressman John Lewis, Dr. Gwen Patton, Mrs. Christine King Farris, Mrs. Coretta Scott King, Brenda and Les Carter, and others.When we arrived in Atlanta, Georgia, 9 days later, my attitude toward nonviolence was beginning to shift, however I did not have a full understanding of the philosophy.After my experience on the 'Freedom Ride' I was inspired and confused as to how to fight using Kingian Nonviolence. 

I continued experimenting with the concept in every phase of conflict and as I continued my study and application of Kingian Nonviolence in policing, my assignments changed from District Commander to Commander of Homicide/Rape and Child Abuse to Commander to Narcotics.I continued to study and grow in the knowledge through Dr. LaFayette's and David Jehnsen's tutelage.I applied the philosophy effectively in interrogating criminal suspects, organizing communities to address the cause of violence and narcotics, empowerment of communities to identify and work on problems. 
As Community policing and Community Orientated Policing became the buzz work in policing, I realized that the Kingian Nonviolence compliments and enhances the concept.

> Power at its best is the right use of strength - Martin Luther King, Jr.

After years of successful application of Kingian Philosophy in policing, I retired from the St. Louis Police Department in 1991 and was asked by Mrs. Coretta Scott King to join the Dr. King Center for Nonviolent Social Change, Inc. in Atlanta, Georgia, and in January 1992 I joined the center's training staff.I continued in the training of police officers and in 1995 was appointed Director of Education and Training for The King Center.

My experience as the Director of Education and Training for The King Center and LaFayette and Associates, which I am working with at the present time, has afforded me the opportunity to conduct training in the following police departments: Atlanta, Georgia; St. Louis, Missouri; Beaumont, Texas; Detroit, Michigan; South Carolina Criminal Justice Academy; Providence, Rhode Island; Schenectady, New York; Oakland, California; Beverly Hills, California; Metro-Dade, Florida; Miami, Florida; Tallahassee, Florida; and Nashville, Tennessee.

The strategy of LaFayette and Associates is to give police agencies the capacity to continue the training within their own agency.Thus, a Training-of-Trainers approach has been designed which consists of 160 hours of training, after which Certification is presented to officers. This qualifies them to teach the two-day Kingian Nonviolence Core Curriculum. The approach has been received very well by police agencies, and it is the first step toward institutionalizing the philosophy in police agencies. 

The training/education of Kingian Nonviolence to police officers is done basically by Dr. LaFayette, David Jehnsen and myself. We encourage other officers who have been Certified to serve as co-trainers with us. In every police training, resistance to this philosophy is a normal first reaction.We encourage questions and require officers to read Dr. King's explanation of his philosophy.After our standard two-day core curriculum, most officers make a complete turn-around.I think some of the major reasons for resistance are: 
1. The word nonviolence is misunderstood
2. They think this is another training criticizing everything they do
3. They think that Dr. King was about over-throwing whites

In 1990 we assembled approximately 30 police officers in Albany, NY. They were Sergeants and above who trained in their respective police department.With this experience Dr. LaFayette, Jr., David Jehnsen and I developed the "Law Enforcement Workbook", designed specifically for training police officers in Kingian Nonviolence.
The philosophy of Kingian Nonviolence as developed by Dr. King is explained in his book, "Stride Toward Freedom", chapter VI, Pilgrimage to Nonviolence.He explains in detail the six Principles of Kingian Nonviolence:
1. Principle 1: Nonviolence is a way of life for courageous people
1. Principle 2: The beloved community is the goal
1. Principle 3: Attack forces of evil, not persons doing evil
1. Principle 4: Accept suffering without retaliation for the sake of the cause to achieve a goal
1. Principle 5: Avoid internal violence of the spirit as well as external physical violence
1. Principle 6:The Universe is on the side of justice

The six steps of Kingian Nonviolence, the strategy, is explained in Dr. King's Letter from a Birmingham Jail.  
1. Step 1 : Information Gathering
1. Step 2 : Education
1. Step 3 : Personal Commitment
1. Step 4 : Negotiation
1. Step 5 : Direct Action
1. Step 6 : Reconciliation

In our police training we refer to the Six Principles as the will and the Six Steps as the skill.The Law Enforcement Workbook, written by Dr. Bernard LaFayette, Jr., David Jehnsen and myself, is used for all police training in Kingian Nonviolence. The Six Principles and Six Steps are articulated in the following police terminology: 
The Six Principles: 

1. Principle 1 : Police officers exhibit acts of courage on a daily basis.Nonviolence requires that one makes a self-analysis of his or her participation in the conflict.
1. Principle 2 : Police officers generally join the law enforcement profession to help people and serve the community. Law enforcement policy and professionalism have always supported the improvement of the total community. 
1. Principle 3 : In reconciling conflicts it is important to recognize the difference between the actions of the person and the issue of the conflict and to direct one's energy to the real problem. Nonviolence requires that we respect all persons humanity and not take their actions personally. 
1. Principle 4 : Sacrifices must be made to achieve most goals.Nonviolence requires a goal, sacrifice without a goal is victimization. 
1. Principle 5 : Nonviolence is an emotional education, requiring one to educate his or her emotions.Negative feelings for people or a group can create internal violence on yourself. 
1. Principle 6 : In the universe there is an inherent need for balance and order. Most people oppose wrong and unjust behavior.

The Six Steps: 

1. Step 1 : Facts from both sides of a conflict can increase police officers' capacity to develop an effective solution. Quality information can be a powerful resource for the police officer.
1. Step 2 : Nonviolence organizes to bring a better understanding of conflicts.This can help solutions become viable and just. Additional resources can be provided to police officers from people who have not taken a position on the issues.
1. Step 3 : Nonviolence requires police officers to develop a clearer understanding of their emotional involvement in a conflict. Police officers understanding the mores and cultural experiences of others can be an asset for reconciling differences.
1. Step 4 : Nonviolence seeks a partial win for all parties involved in the conflict without compromising one's sworn duty.Police officers often are involved in negotiations, but when it's done with nonviolent skills it can be very effective.
1. Step 5 : Nonviolence intervention can unlock a polarized conflict.Constructive intervention can help create new options for reconciliation.
1. Step 6 :Persons involved in the problem must be involved in the solution.Nonviolence helps police officers look at old problems with new possibilities.

The states of Florida and Michigan have the most police officers who are certified to teach the two-hour core curriculum of Kingian Nonviolence.In Florida, the Florida Martin Luther King, Jr. Institute, has taken the lead in police training, where we just completed an eight hour Kingian Nonviolence training for every sworn officer in the Metro-Dade Police Department, for over 3,000 police officers.Some of the comments about the application of Kingian Nonviolence in policing are: 
I worked with Capt. Alphin in the St. Louis Police Department and saw the effective use of the philosophy in managing conflicts and mobilizing the community.I recognize the impact of the philosophy, it builds bridges, not walls.I remember Capt. Alphin saying, we must win them over, win over them, I practice the philosophy today, it works.

"I have been a Certified Trainer in Kingian Nonviolence for over 3 years, and have successfully applied this philosophy in my management of police officers and addressing community conflicts.I am committed to the Kingian Philosophy and intend to continue teaching my fellow police officers."- Lieutenant Troy Quinn, Detroit Police Department, Detroit, Michigan

"I was skeptical of the philosophy when I first heard it presented, as most of the officers were in my class.As I continued to listen, I began to see how it could be used in all phases of police work.Since I have been Certified, I have used the philosophy in all types of police and community conflicts.I definitely think this philosophy is necessary in policing for the 21st Century."- Sergeant Tonya King, Providence Police Department, Rhode Island

Since my Certification in Kingian Nonviolence, the philosophy has enhanced and enriched my professional and private life.I have had occasions to apply it to serious conflicts with police officers and the community. Being assigned to the Training Academy and having the opportunity to observe officers receive the training, I have seen its impact on officers of all cultures.

I did not want to go to the training or be trained as a trainer in Kingian Nonviolence.I was very resistant the first few days.After I began to listen and understand the philosophy, I began to see how it could be applied in policing.All police officers should be trained in this philosophy as it helps us address all conflicts with a methodology that has been successful for me. 

As we approach the 21st Century will we make the same mistakes we made in the 20th Century?What are the future implications of nonviolence in policing?Can we create a nonviolence police force without creating a nonviolent government?Can we create a nonviolent police force that will be effective without teaching nonviolence in the educational system?To what degree does violence on television, cartoons, movies, news, individual and sitcoms, affect the attitude and behavior of people?

Kingian Nonviolence holds a great potential for the future, particularly in policing.However, policing can not stand alone, nonviolence in the school, home, streets and every phase of our life, was Dr. Martin Luther King, Jr's. dream.

> Now the time has come for man to experiment with nonviolence in all areas of human conflict, and that means nonviolence on an international scale. Martin Luther King, Jr