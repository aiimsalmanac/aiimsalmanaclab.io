---
layout: post
title:  "Kramdown reference guide"
author: Ayat
author-bio: "Ayat is an newest member of the family. She took birth hardly half hour ago."
author-avatar: assets/images/f.png
author-twitter: https://twitter.com/the_hindu
categories: [ markdown, tutorial ]
image: assets/images/3.jpg
tags: featured
---

<div class="post-content -cheatsheet">
<h3 id="configuration">Configuration</h3>
<ul>
<li><code>parse_block_html</code> - process kramdown syntax inside&nbsp;blocks</li>
<li><code>parse_span_html</code> - process kramdown syntax inside&nbsp;inlines</li>
<li>
<p><code>html_to_native</code> - convert html elements to native&nbsp;elements</p>
</li>
</ul>
<p>For the GFM&nbsp;parser:</p>
<ul>
<li><code>hard_wrap</code></li>
</ul>
<p>http://kramdown.gettalong.org/parser/gfm.html</p>
<h3 id="for-jekyll-gh-pages">For jekyll&nbsp;(gh-pages)</h3>
<pre><code class="hljs makefile"><span class="hljs-comment"># _config.yml</span>
markdown: kramdown
<span class="hljs-title">kramdown:</span>
  input: GFM
</code></pre>
<h3 id="footnotes-kramdown">Footnotes&nbsp;(Kramdown)</h3>
<pre><code class="hljs css"><span class="hljs-tag">This</span> <span class="hljs-tag">is</span> <span class="hljs-tag">some</span> <span class="hljs-tag">text</span>.<span class="hljs-attr_selector">[^1]</span>. <span class="hljs-tag">Other</span> <span class="hljs-tag">text</span>.<span class="hljs-attr_selector">[^footnote]</span>.

<span class="hljs-attr_selector">[^1]</span>: <span class="hljs-tag">Some</span> *<span class="hljs-tag">crazy</span>* <span class="hljs-tag">footnote</span> <span class="hljs-tag">definition</span>.
</code></pre>
<h3 id="abbreviations-kramdown">Abbreviations&nbsp;(Kramdown)</h3>
<pre><code class="hljs coffeescript">This <span class="hljs-keyword">is</span> some text <span class="hljs-keyword">not</span> written <span class="hljs-keyword">in</span> HTML but <span class="hljs-keyword">in</span> another language!

*[another language]: It<span class="hljs-string">'s called Markdown
*[HTML]: HyperTextMarkupLanguage
</span></code></pre>
<h3 id="classes-and-ids-kramdown">Classes and IDs&nbsp;(Kramdown)</h3>
<pre><code class="hljs ruby"><span class="hljs-constant">A</span> simple paragraph with an <span class="hljs-constant">ID</span> attribute.
{<span class="hljs-symbol">:</span> <span class="hljs-comment">#para-one}</span>

&gt; <span class="hljs-constant">A</span> blockquote with a title
{<span class="hljs-symbol">:title=<span class="hljs-string">"The blockquote title"</span></span>}
{<span class="hljs-symbol">:</span> <span class="hljs-comment">#myid}</span>

* {<span class="hljs-symbol">:</span>.cls} <span class="hljs-constant">This</span> item has the <span class="hljs-class"><span class="hljs-keyword">class</span> "<span class="hljs-title">cls</span>"</span>

{<span class="hljs-symbol">:</span>.ruby}
    <span class="hljs-constant">Some</span> code here
</code></pre>
<h3 id="references">References</h3>
<ul>
<li>http://kramdown.gettalong.org/syntax.html</li>
<li>http://kramdown.gettalong.org/parser/kramdown.html</li>
</ul>
</div>