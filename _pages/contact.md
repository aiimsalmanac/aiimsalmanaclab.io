---
title: "Contact"
permalink: "/contact.html"
---

<p class="mb-4">Please send your message to {{site.name}}. We will reply as soon as possible!</p>
<a href="mailto:aiimsalmanac@gmail.com?Subject=AIIMS%20Almanac" target="_top">aiimsalmanac@gmail.com</a>

