---
title: "About"
layout: page-sidebar
permalink: "/about.html"
image: "/assets/images/screenshot.jpg"
comments: true
---

The following software stack is used in creation of this blog as of 02 March, 2020

- [Jekyll](https://jekyllrb.com/) as Static Site Generator
- [Gitlab](https://about.gitlab.com/) for hosting and Continous integration
- [Mundana Theme for Jekyll](https://github.com/wowthemesnet/mundana-theme-jekyll) for theme
- [Git](https://git-scm.com/) for Version Control
- [Kramdown](https://kramdown.gettalong.org/) flavor of [Markdown](https://daringfireball.net/projects/markdown/) for markup

Credits are due to the following websites for images used in this blog
- [Wallpaperswide.com](http://wallpaperswide.com/)
- [WallpapersMug.com](https://wallpapersmug.com/)
- [CompressJPEG.com](https://compressjpeg.com/)